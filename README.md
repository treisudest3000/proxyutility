# ProxyScrape Utility

ProxyScrape Utility uses the [ProxyScrape](https://proxyscrape.com) API to download 
proxies, this tool just makes it easier to just open it from your desktop.

It is pretty easy to use it, you just need to open it, select what **type** of
proxy do you want to scrape, there are three types:

1. *HTTP* Proxies
2. *Socks4* Proxies
3. *Socks5* Proxies

You can choose the one you need. The **HTTP Proxies** are more complex, because
you can select if you want to have a SSL *(secure socket layer)* or if you want
to be anonymous.

There are the following **SSL** options:

- Default *(all)*
- On *(yes)*
- Off *(no)*

Also these are the following anonimity options:

- Default *(all)*
- Elite *(elite)*
- Anonymous *(anonymous)*
- Transparent *(transparent)*

> The text in the brackets are the API parameters.

# Compiled version download

If you want you can download an already compiled version [here](https://file-link.net/51628/proxyutility)