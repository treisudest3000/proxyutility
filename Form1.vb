﻿Imports System.Environment
Imports System.IO
Imports System.Net

Public Class Form1
    Dim ssl As String = "all"
    Dim anonimity As String = "all"
    Dim type As String = "http"
#Region " Move Form "

    ' [ Move Form ]
    '
    ' // By Elektro 

    Public MoveForm As Boolean
    Public MoveForm_MousePosition As Point

    Public Sub MoveForm_MouseDown(sender As Object, e As MouseEventArgs) Handles _
    MyBase.MouseDown ' Add more handles here (Example: PictureBox1.MouseDown)

        If e.Button = MouseButtons.Left Then
            MoveForm = True
            Me.Cursor = Cursors.NoMove2D
            MoveForm_MousePosition = e.Location
        End If

    End Sub

    Public Sub MoveForm_MouseMove(sender As Object, e As MouseEventArgs) Handles _
    MyBase.MouseMove ' Add more handles here (Example: PictureBox1.MouseMove)

        If MoveForm Then
            Me.Location = Me.Location + (e.Location - MoveForm_MousePosition)
        End If

    End Sub

    Public Sub MoveForm_MouseUp(sender As Object, e As MouseEventArgs) Handles _
    MyBase.MouseUp ' Add more handles here (Example: PictureBox1.MouseUp)

        If e.Button = MouseButtons.Left Then
            MoveForm = False
            Me.Cursor = Cursors.Default
        End If

    End Sub

#End Region
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        WindowState = FormWindowState.Minimized
    End Sub

    Private Sub BunifuFlatButton1_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton1.Click
        DisplayProxy()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
    Public Function DisplayProxy()
        ListBox1.Items.Clear()
        Dim appdata = GetFolderPath(SpecialFolder.ApplicationData)
        If My.Computer.FileSystem.FileExists(appdata + "/proxies.txt") Then
            My.Computer.FileSystem.FileExists(appdata + "/proxies.txt")
            Dim fs As FileStream = File.Create(appdata + "/proxies.txt")
            fs.Close()
        Else
            Dim fs As FileStream = File.Create(appdata + "/proxies.txt")
            fs.Close()
        End If
        Dim text2() As String
        If type = "http" Then
            Dim web As New WebClient
            Dim url As String = "https://api.proxyscrape.com/?request=displayproxies&proxytype=" + type + "&timeout=10000&country=all&anonymity=" + anonimity + "&ssl=" + ssl
            Dim string2 As String = web.DownloadString(url)
            Dim streamer As StreamWriter
            streamer = New StreamWriter(appdata + "/proxies.txt")
            streamer.WriteLine(string2)
            streamer.Close()
            Dim text() As String = IO.File.ReadAllLines(appdata + "/proxies.txt")
            text2 = text
            ListBox1.Items.AddRange(text)
        ElseIf type = "socks4" Then
            Dim web As New WebClient
            Dim url As String = "https://api.proxyscrape.com/?request=displayproxies&proxytype=socks4&timeout=10000&country=all"
            Dim string2 As String = web.DownloadString(url)
            Dim streamer As StreamWriter
            streamer = New StreamWriter(appdata + "/proxies.txt")
            streamer.WriteLine(string2)
            streamer.Close()
            Dim text() As String = IO.File.ReadAllLines(appdata + "/proxies.txt")
            text2 = text
            ListBox1.Items.AddRange(text)
        ElseIf type = "socks5" Then
            Dim web As New WebClient
            Dim url As String = "https://api.proxyscrape.com/?request=displayproxies&proxytype=socks5&timeout=10000&country=all"
            Dim string2 As String = web.DownloadString(url)
            Dim streamer As StreamWriter
            streamer = New StreamWriter(appdata + "/proxies.txt")
            streamer.WriteLine(string2)
            streamer.Close()
            Dim text() As String = IO.File.ReadAllLines(appdata + "/proxies.txt")
            text2 = text
            ListBox1.Items.AddRange(text)
        End If
        Button3.Enabled = True
    End Function

    Private Sub TrackBar1_Scroll(sender As Object, e As EventArgs) Handles TrackBar1.Scroll
        If TrackBar1.Value = 0 Then
            ssl = "all"
        ElseIf TrackBar1.Value = 1 Then
            ssl = "yes"
        ElseIf TrackBar1.Value = 2 Then
            ssl = "no"
        End If
    End Sub

    Private Sub TrackBar2_Scroll(sender As Object, e As EventArgs) Handles TrackBar2.Scroll
        If TrackBar2.Value = 0 Then
            anonimity = "all"
        ElseIf TrackBar2.Value = 1 Then
            anonimity = "elite"
        ElseIf TrackBar2.Value = 2 Then
            anonimity = "anonymous"
        ElseIf TrackBar2.Value = 3 Then
            anonimity = "transparent"
        End If
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then
            GroupBox2.Enabled = True
            type = "http"
            GroupBox2.Cursor = Cursors.Arrow
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton2.Checked = True Then
            GroupBox2.Enabled = False
            type = "socks4"
            GroupBox2.Cursor = Cursors.No
        End If
    End Sub

    Private Sub RadioButton3_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton3.CheckedChanged
        If RadioButton3.Checked = True Then
            GroupBox2.Enabled = False
            type = "socks5"
            GroupBox2.Cursor = Cursors.No
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        SaveFileDialog1.ShowDialog()
    End Sub

    Private Sub SaveFileDialog1_FileOk(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles SaveFileDialog1.FileOk
        My.Computer.FileSystem.CopyFile(GetFolderPath(SpecialFolder.ApplicationData) + "/proxies.txt", SaveFileDialog1.FileName)
    End Sub
End Class
